import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';

import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { EmpbyidComponent } from './empbyid/empbyid.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';

//Routing through ts
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    RegisterComponent,
    ShowemployeesComponent,
    EmpbyidComponent,
    ExpPipe,
    GenderPipe,
    ProductsComponent,
    LogoutComponent,
    HeaderComponent     //Routing through ts
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
