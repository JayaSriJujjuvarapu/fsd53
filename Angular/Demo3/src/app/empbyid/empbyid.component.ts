import { Component } from '@angular/core';

@Component({
  selector: 'app-empbyid',
  templateUrl: './empbyid.component.html',
  styleUrl: './empbyid.component.css'
})
export class EmpbyidComponent {
  empId: any;
  emp: any;
  employees: any;

  //DateFormat: MM-dd-YYYY
  constructor() {
    this.employees = [
      {empId:101, empName:'Harsha',  salary:1212.12, gender:'Male',   doj:'05-25-2018', country:"IND", emailId:'harsha@gmail.com',  password:'123'},
      {empId:102, empName:'Pasha',   salary:2323.23, gender:'Male',   doj:'06-26-2017', country:"USA", emailId:'pasha@gmail.com',   password:'123'},
      {empId:103, empName:'Indira',  salary:3434.34, gender:'Female', doj:'07-27-2016', country:"CHI", emailId:'indira@gmail.com',  password:'123'},
      {empId:104, empName:'Vamshi',  salary:4545.45, gender:'Male',   doj:'08-28-2015', country:"JAP", emailId:'vamshi@gmail.com',  password:'123'},
      {empId:105, empName:'Krishna', salary:5656.56, gender:'Male',   doj:'09-29-2014', country:"UK",  emailId:'krishna@gmail.com', password:'123'}
    ];
  }

  ngOnInit() {
  }

  getEmployee() {
    this.emp = null;
    
    this.employees.forEach((element: any) => {
      if (element.empId == this.empId) {
        this.emp = element;
      }
    });
  }
}


