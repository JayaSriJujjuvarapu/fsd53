import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { EmpbyidComponent } from './empbyid/empbyid.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';

//Provide the Routing paths with their components
const routes: Routes = [
  {path:'', component:LoginComponent}, 
  {path:'login', component:LoginComponent}, 
  {path:'register', component:RegisterComponent},
  {path:'showemps', component:ShowemployeesComponent},  
  {path:'empbyid', component:EmpbyidComponent}, 
  {path:'products', component:ProductsComponent},
  {path:'logout', component:LogoutComponent} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
