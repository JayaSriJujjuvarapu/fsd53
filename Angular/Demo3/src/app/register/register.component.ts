import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {

  employee: any;

  constructor() {
    this.employee = {
      empId: '', 
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      deptId: ''
    }
  }

  ngOnInit(): void {
  }

  submit() {
    console.log(this.employee);
  }
  registerSubmit(regForm: any) {
    console.log(regForm);
}

}