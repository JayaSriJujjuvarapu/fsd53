import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrl: './test2.component.css'
})
export class Test2Component implements OnInit{
  person:any;
constructor(){
  //alert("Constructor is invoked");
   console.log("Constructor invoked");
   this.person={id:102,name:"Rajasekhar",age:22,address:{streetno:1201,city:"Hyd",state:"Telangana"},hobbies:["Sleeping","Watching movies","Playing"]};
}
ShowDetails(){
  console.log(this.person);
}
ngOnInit(): void {
  //alert("ngOnInit invoked");
  console.log("ngOnInit invoked");
}
}
