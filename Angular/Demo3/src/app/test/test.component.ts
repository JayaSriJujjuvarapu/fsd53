import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {
    id:number;
    name:string;
    age:number;
    address:any;
    hobbies:any;
    constructor(){
     this.id=101;
     this.name="jayasri";
     this.age=21;
     this.address={streetno:1201,city:"Hyd",state:"Telangana"};
     this.hobbies=["Sleeping","Watching movies","Playing"];
        }
}
