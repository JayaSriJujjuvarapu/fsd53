import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-test',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './test.component.html',
  styleUrl: './test.component.css'
})
export class TestComponent {
   id:number;
   name:string;
   age:number;
   address:any;
   hobbies:any;
   constructor(){
    this.id=101;
    this.name="Jayasri";
    this.age=21;
    this.address={streetno:101,
      city:'Hyd',
      state:'Telangana'};
      this.hobbies=['Sleeping','Watchingmovies','scrollinginsta'];
   }
   
}
