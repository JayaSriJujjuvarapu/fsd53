package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDAO {
@Autowired
ProductRepository prodRepo;
public List<Product> getAllProducts(){
	return prodRepo.findAll();
}
public Product getProductById(int productid) {
	 Product Product= new Product(0, "Product Not Found!!!", 0.0);
	    return prodRepo.findById(productid).orElse(Product);
}
public Product getProductByName(String prodName) {
	 return prodRepo.findByName(prodName);
}
public Product addProduct(Product product) {
    return prodRepo.save(product);
}
public Product updateProduct(Product product) {
    return prodRepo.save(product);
}
public void deleteProductById(int productid) {
    prodRepo.deleteById(productid);
}

}
